#!/usr/bin/env ruby
# coding: utf-8

print "Standby..."

require 'bundler'
Bundler.require

include PiPiper

pin = Pin.new pin:24, direction: :in, pull: :down

tokens=open("./settings.json") do |data|
   JSON.load(data)
end

client = Twitter::REST::Client.new do |config|
   config.consumer_key        = tokens['consumer_key']
   config.consumer_secret     = tokens['consumer_secret']
   config.access_token        = tokens['access_token']
   config.access_token_secret = tokens['access_token_secret']
end

puts "OK!"

loop do
   pin.read
   client.update("にゃーん") if pin.changed?
   sleep 0.3
end

